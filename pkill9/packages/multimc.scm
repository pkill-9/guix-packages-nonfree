;; for each MC instance with forge installed, set 'enabled=false' in config/splash.properties because with nouveau it either crashes the game or freezes X (mouse still moves though) and i have to hard-reset the system.
(define-module (pkill9 packages multimc)
  #:use-module (gnu packages)
  #:use-module (gnu packages java)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (guix licenses)
 )

(define-public multimc
(package
  (name "multimc")
  (version "0.6.2")
  (source
    (origin
      (method git-fetch)
     (uri (git-reference
            (url "https://github.com/MultiMC/MultiMC5")
            (commit "4cbd1a7692ba71c91077aaf60663f1504724f497")
            (recursive? #t)))
      (sha256
        (base32
          "10wqrs3nbbdfpxsyx19qyq5q2jl25z6jjnjknm96id9y76h21k1k"))))
  (build-system cmake-build-system)
  (native-inputs
   `(("icedtea:jdk" ,icedtea-8 "jdk")))
  (inputs
   `(("qtbase" ,(@ (gnu packages qt) qtbase))
     ("icedtea" ,(@ (gnu packages java) icedtea-8))
     ("xrandr" ,(@ (gnu packages xorg) xrandr))
     ("zlib" ,(@ (gnu packages compression) zlib))
     ("mesa" ,(@ (gnu packages gl) mesa))
     ("pulseaudio"
      ,(@ (gnu packages pulseaudio) pulseaudio))
     ("libxrandr" ,(@ (gnu packages xorg) libxrandr))
     ("libx11" ,(@ (gnu packages xorg) libx11))
     ("libxext" ,(@ (gnu packages xorg) libxext))
     ("libxcursor" ,(@ (gnu packages xorg) libxcursor))
     ("libxxf86vm" ,(@ (gnu packages xorg) libxxf86vm))
     ))
  (arguments
   `(#:configure-flags (list "-DMultiMC_LAYOUT=lin-system") ;; set FHS layout
     #:tests? #f ;; DownloadTask test fails
     #:phases
     (modify-phases %standard-phases
       (add-after 'build 'install-desktop-entry-and-icon
         (lambda* (#:key outputs #:allow-other-keys)
           ;; Add .desktop files for MultiMC
           (let* ((output (assoc-ref outputs "out"))
		              (source (assoc-ref %build-inputs "source"))
                  (apps (string-append output "/share/applications")))
             (mkdir-p apps)
             (with-output-to-file
                 (string-append apps "/multimc.desktop")
               (lambda _
                 (format #t
                         "[Desktop Entry]~@
                   Name=MultiMC~@
                   Comment=Minecraft launcher~@
                   Exec=~a/bin/multimc~@
                   TryExec=~@*~a/bin/multimc~@
                   Icon=multimc~@
                   Categories=Game;Simulation~@
                   Type=Application~%"
                         output)))
	     (mkdir-p (string-append output "/share/icons/hicolor/scalable/apps"))
	     (copy-file (string-append source "/application/resources/multimc/scalable/multimc.svg")
			(string-append output "/share/icons/hicolor/scalable/apps/multimc.svg"))
             #t)))
       (add-after 'install-binaries 'wrap-binaries
         (lambda* (#:key outputs inputs #:allow-other-keys)
           (let* ((out (assoc-ref outputs "out"))
                  (bin (string-append out "/bin/multimc"))
                  (xrandr (assoc-ref inputs "xrandr"))
                  (icedtea (assoc-ref inputs "icedtea"))
                  (pulseaudio (assoc-ref inputs "pulseaudio"))
                  (mesa (assoc-ref inputs "mesa"))
                  (libx11 (assoc-ref inputs "libx11"))
                  (libxext (assoc-ref inputs "libxext"))
                  (libxcursor (assoc-ref inputs "libxcursor"))
                  (libxxf86vm (assoc-ref inputs "libxxf86vm"))
                  (libxrandr (assoc-ref inputs "libxrandr")))
             (wrap-program bin
               `("GAME_LIBRARY_PATH" ":" prefix
                 (,(string-append mesa "/lib:" libx11 "/lib:" libxext "/lib:" libxcursor "/lib:" libxxf86vm "/lib:" pulseaudio "/lib:" libxrandr "/lib")))
               `("PATH" ":" prefix
                 (,(string-append xrandr "/bin:" icedtea "/bin"))))
	     ; use ~/.multimc by default.
                (substitute* bin
                            (("\\$@") "-d\" \"$HOME/.multimc\" \"$@"))
             #t))))))
  (native-search-paths
    (list (search-path-specification
	    (variable "XDG_DATA_DIRS")
	    (files '("share")))))
  (home-page "https://multimc.org")
  (synopsis "A free, open source launcher for Minecraft")
  (description "Allows you to have multiple, separate instances of Minecraft (each with their own mods, texture packs, saves, etc) and helps you manage them and their associated options with a simple interface.")
  (license #f))
)
