(define-module (pkill9 packages mosaic)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages))

(define-public mosaic
  (package
   (name "mosaic")
   (version "2.7")
   (source (origin
	    (method git-fetch)
	    (uri (git-reference
		  (url "https://github.com/alandipert/ncsa-mosaic")
		  (commit "fd11fb7c900e850ff24b6b8c6548324190c335ac")))
	    (file-name (git-file-name name version))
	    (sha256
	     (base32
	      "12wzsn3g307750xgq6mfq1f02vhp16ki7a31m3c270mawq571wqh"))))
   (build-system gnu-build-system)
   (inputs
    `(("libjpeg" ,(@ (gnu packages image) libjpeg))
      ("libpng" ,(@ (gnu packages image) libpng))
      ("libxpm" ,(@ (gnu packages xorg) libxpm))
      ("libxt" ,(@ (gnu packages xorg) libxt))
      ("libxmu" ,(@ (gnu packages xorg) libxmu))
      ("lesstif" ,(@ (gnu packages lesstif) lesstif))
      ("libxext" ,(@ (gnu packages xorg) libxext))))
   (arguments
    `(#:tests? #f
      #:make-flags (list "DEV_ARCH=linux")
      #:phases (modify-phases %standard-phases
                              (delete 'configure)
			      (replace 'install
			       (lambda _
				 (install-file "src/Mosaic"
					       (string-append (assoc-ref %outputs "out") "/bin")))))))
   (home-page "https://github.com/alandipert/ncsa-mosaic")
   (synopsis "One of the first graphical web browsers.")
   (description "One of the first graphical web browsers.")
   (license "custom")))

(package (inherit mosaic))
