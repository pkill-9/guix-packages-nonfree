; todo: remove -Werror from makefile.in files (these are what configure uses to generate i think, according to http://stackoverflow.com/questions/2531827/ddg#2531841)
(define-module (pkill9 packages steam additional-steam-libs libindicator)
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (guix licenses)
 )

; Example arguments to use:
;    (arguments
;     `(#:tests? #f
;       #:make-flags '("CC=gcc" "RM=rm" "SHELL=sh" "all")
;       #:phases (modify-phases %standard-phases (delete 'configure))
;       ))

(define-public libindicator
(package
  (name "libindicator")
  (version "12.10.1")
  (source
    (origin
      (method url-fetch)
      (uri (string-append
             "https://launchpad.net/libindicator/12.10/"
             version
             "/+download/libindicator-"
             version
             ".tar.gz"))
      (sha256
        (base32
          "0zs4z7l9b57jldwz0ban77f3c2zq43ambd0dssf5qg9i216f9lmj"))))
  (build-system gnu-build-system)
  (native-inputs
    `(("pkg-config"
       ,(@ (gnu packages pkg-config) %pkg-config))))
  (inputs
    `(("gtk+" ,(@ (gnu packages gtk) gtk+-2))
      ("gobject-introspection"
       ,(@ (gnu packages glib) gobject-introspection))
      ("glib:bin" ,(@ (gnu packages glib) glib) "bin")
      ("glib" ,(@ (gnu packages glib) glib))))
  (arguments
   `(#:configure-flags '("--with-gtk=2")
     #:tests? #f ; tests keep failing due to warnings, so shrug
     #:phases (modify-phases %standard-phases
                             (add-after 'unpack 'fix-typo
                                        (lambda _
                                        ; apply fix for typo here: https://bbs.archlinux.org/viewtopic.php?pid=1568469#p1568469
                                          (substitute* "libindicator/Makefile.am"
                                                      (("-Werror") "-Wno-deprecated-declarations"))
                                          (substitute* "libindicator/Makefile.in"
                                                      (("-Werror") "-Wno-deprecated-declarations"))
                                          (substitute* "configure"
                                                       (("LIBINDICATOR_LIBS\\+\\=.......") "LIBINDICATOR_LIBS+=\" $LIBM\""))))
                             )))
  (home-page "")
  (synopsis "")
  (description "")
  (license gpl3+))

 )
