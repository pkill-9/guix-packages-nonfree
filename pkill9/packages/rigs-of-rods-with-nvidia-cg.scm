(define-module (pkill9 packages rigs-of-rods-with-nvidia-cg)
  #:use-module (pkill9 packages rigs-of-rods rigs-of-rods) ; for ror - from the 'free' repo.
  #:use-module (pkill9 packages rigs-of-rods ogre-next) ; For ogre-next's package version - will not need this, this is currently to avoid recompiling
  #:use-module (pkill9 packages nvidia-cg-toolkit)
  #:use-module (guix build-system cmake)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (srfi srfi-1) ; for 'fold'
 )


(define ogre-ror-with-cg ; RoR's custom ogre, with the addition of nvidia-cg-toolkit
  (package (inherit ogre-ror)
    (inputs
      `(,@(package-inputs ogre-ror)
         ("nvidia-cg-toolkit" ,nvidia-cg-toolkit)))
    ))

(define ogre-ror-with-cg-graft
  (package (inherit ogre-ror)
    (replacement ogre-ror-with-cg)))

(define ogre-caelum ;; Optional dependency for RoR that requires nvidia-cg to function as all it's shaders are written with nvidia-cg.
  (package
    (name "ogre-caelum")
    (version "ror-git")
    (source
        (origin
          (method git-fetch)
         (uri (git-reference
                (url "https://github.com/RigsOfRods/ogre-caelum.git")
                (commit "f24279b57f863ed6d59a591b6d7c0d6168bfa2a9")))
          (sha256
            (base32
             "0xwmhwg6nxqkw7cl29n6464vnd1dzljkfpr02ai2da4zgqvq9x51"))))
    (build-system cmake-build-system)
    (inputs
      `(("ogre" ,ogre-ror-with-cg)))
    (arguments
     `(#:tests? #f)) ; No tests I assume
    (home-page "http://wiki.ogre3d.org/Caelum")
    (synopsis "Caelum is a plug-in/library for Ogre targeted to help create nice-looking (photorealistic if possible) atmospheric effects such as sky colour, clouds and weather phenomena such as rain or snow.")
    (description "Caelum is a plug-in/library for Ogre targeted to help create nice-looking (photorealistic if possible) atmospheric effects such as sky colour, clouds and weather phenomena such as rain or snow.")
    (license #f)))

(define with-nvidia-cg
  (package-input-rewriting
    `((,ogre-ror . ,ogre-ror-with-cg-graft))
    (lambda (name)
      (if (equal? name "rigs-of-rods")
	  "rigs-of-rods-with-nvidia-cg" name))))

(define-public rigs-of-rods-with-nvidia-cg
  (with-nvidia-cg rigs-of-rods))
